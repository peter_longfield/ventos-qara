# Ventos QA RA

Repository for [Project VentOS](https://gitlab.com/project-ventos/ventos)
Quality Assurance and Regulatory Approval documents.

This repository is currently experimental and the source of truth for QARA
project document is [this Google drive
folder](https://drive.google.com/drive/folders/1wcTF7E1ZnuGMVU5yjoEONSTjGIWprGBP?usp=sharing).


