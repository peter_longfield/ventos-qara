# Meeting Minutes

## 12pm Sydney Time, 19 August 2020

Attendees: Dr. Erich Schulz, Ben Coombs, Pierre Lonchampt

Purpose: Quality planning for VentOS

## Notes:

- Design reviews
  - clear id of who was in the 'room' with ability to raise issues, what was discussed, what was decided
  - async - lots of volunteers and stakeholders
  - ISO 13485
  - 7.3.5 Design and dev review
  - What are the 'suitable stages'
  - 'Systemic reviews' - plan and methodology, Chairman, set audience
  - What is our system?
  - eg. everyone in the same room at the same time. Difficult to implement in timezones.
  - Sub-teams could meet with their own topics.
  - Higher level meetings with a representative from each group for a top level decision.
  - Designing our own system - traceability.
  - Synchronous - easy to put a timestamp on a decision.
  - Difficult to document async decision making and coding.
  - Code reviews and check ins.
  - 13485 - design control
  - 62304 - software lifecycle process
  - changing codebase 6.2.1.3
  - systematic procedure
  - who is in charge of reviewing and approving (2 different responsibilities) but can be done by the same person
  - reviewer needs technical expertise of the language and domain - can peer-review    
  - approval is managerial role to approve the change (non-technical role)


- Quality Plan - can be multiple documents
  - “Our process is to use Git, each development of a feature in a separate branch, named with this convention, review done by someone with reviewer authority, merging is done by approver.”
  - Make any rules clear - eg. 2 people (although this may be too restrictive)
  - Generally want a single approver with a deputy person as backup; approver can request
  - as of this date this person is the:
    - developer
    - reviewer
    - approver
  - This date log can change as the roles change.
  - Regulator will ask:
    1. show me your procedure
    2. show me the records to show you followed the procedure


- IEC 62304
  - 5.5 unit implementation (aka. writing code)
  - 5.5.2/5.5.3 level of code review
  - Safety class C (life saving device) - we must do everything required in the standard to the highest level.
  - Write plan. Review plan (currently doing this now). Sign.
  - 5.2 requirements analysis needs to have a review section. 5.2.6 Process for our requirements to be reviewed (someone independent from the person that wrote the cod).
  - 5.3 - 5.3.6 someone else (not the designer) needs to verify it.
  - Detail design separately needs to be documented and reviewed separately - eg. this feature is implemented in this class with these methods etc.
  - No regulatory rules to write software and requirements in any time order. Rational order. ie. doesn't have to be waterfall.


- Discussion
  - Be honest in the quality plan - urgent, non-profit volunteer program. Limitations. For the code check in, developer will describe the requirements. Be transparent on how you will do this in the plan.
  - Validation - FDA expects all software that is used as a process tool on activities that have an impact on the final code - unit, integration etc.
  - Design a test is another skill above running a test - understand and be critical why you are testing, what you are trying to prevent.
  - Validation starts with risk management. Could I could harm/kill someone with this simple script? Identified and reviewed by a proven specialist in YAML, markdown etc.
  - Why am I testing, what am I trying to validate?
  - Can someone down the line change the script? Once we have validated the script - secure the script, eg. hash YAML to Markdown to validate where the file came from. Or use a manual process.
  - Weigh time saved using automation vs. time to validate something.
  - Document part of the Quality Plan - birdseye technical version of the project. Vague limits of the target platform.
  - Expectation that hardware integration testing needs to be done by hardware teams.
  - Project itself is not a medical device in and of itself. eg. we don't need to exactly comply with 62304.
  - Documenting requirements - don't bother at this stage to document our individual requirements.
  - Spend more time documenting scope, goals and boundaries, eg. what are the platforms we could work with.
  - Level of interaction, sensor readings, what messages do we provide.
  - Code check ins - white box
  - Black box description - here are our limits and we are doing this black box stuff that you don't need to know about.
  - Licensing challenge not so much FDA approval.
  - What processes does the other team have in place to guarantee the safety of the device with the software running on it.
  eg. update processes for the manufacturer.
  - Manufacturer has a duty to report back to FDA. Ethically - was our code involved and was a bug issue that caused harm and we have an ethical duty to report to other projects (downstream users) using that code in their projects.
  - What manufacturers can do, and what they should do.
  - Communicate and collaborate with PolyVent and Victor.
  - OK to leave some sections - but we should say “to be refined” or similar - don’t leave it blank.
  - Pierre happy to talk to PolyVent with us.
  - External documentation how we doing things and what we are doing - an extension of the HE highlighted project piece.
