# Meeting Minutes

## August 25th 2020 5PM PST

Participants: Ben Coombs, Dr Erich Schulz, Robert L. Reid, PhD, Brittany Tran

Duration: 1hours

## Notes:
- Outreach
- ResipraWorks
- gVent
- PolyVent
- Issue #16
  - Functional Programming
  - Question from Erich: How to integrate pure functional code with extensibility
  - Ideally a team provides a page of code on top of VentOS
- Issue #2
  - PIO unit testing vs Minunit
  - Native vs Arduino code not 1:1
  - How to test?
- What are we simulating?
  - Delay in sensor reading (eg. BME280 needs heater to warm up sensor)
  - Simulate existence of the components that are available (eg. 2 solenoids vs 3), doesn’t need to be a full mechanical sim.
- Integration testing still required
- Wide variety of CPUs, sensors, drive mechanism
- Establish API
- Stability - don’t want to be changing it while other teams are using it
- Perhaps 6 weeks development with 1 week sprints
