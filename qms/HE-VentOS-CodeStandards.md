Coding Standards
================

## Firmware Development
All firmware is developed in C++ using the functional programming paradigm.
Please be aware that while all file extensions use C++ the programming method
follows C industry standards for safety critical firmware.
 todo: make this more specific

## Code Rules
### Naming Conventions
my_function_name()

### Memory Allocation
No memory allocation shall be used.

### File Extensions

### Further points for consideration

1. Code must pass lint and have 100% test coverage before merging
1. Perform all time comparison via a central custom function that accounts for
   clock roll-over
1. Where possible use a single source of truth, ideally within YAML
1. Use dependency injection patterns to allow unit testing of code
1. Avoid floating point operations and favour using scaling constants
1. Use good git practices, eg:
    1. rebase and squash commits before doing a pull request
    1. address a single issue in each PR
    1. do not commit generated files to the main repository
1. Organise the continuous integration (CI) pipeline
    1. Decompose build steps into commands runnable via the command line.
    1. Include most of these commands in the makefile.
    1. Reference makefile commnads as much as possible within the CI
       configuration file.
