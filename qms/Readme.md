# VentOS QMS Readme

## Design History File
This repository is the Design History File for [VentOS](https://gitlab.com/project-ventos/ventos).

## Document Structure
- All VentOS 'live' documentation is implemented in Markdown and follows Markdown best practices.
- Documentation shall follow git source control best practices with fully specified commit messages for traceability. Branches shall be used and pull requests shall follow the method from HE-VentOS-QualityPlan Section 9.1.8.
- All images shall be jpg or svg format to minimise file size.
- All images shall be clear and legible.
- Excel files may used in a limited capacity where necessary.
- Excel, image and PDF files are binary files which makes it difficult to track changes using GitLab source control.
    - Image and PDF files shall be versioned using a traditonal numbering format, eg. `my_file_v0.1.pdf`)
    - Excel files shall be versioned internally per row, eg. by adding a date and version column.

## Relevant Standards
IEC 62304 - Medical device software
ISO 14971 - Application of risk management to medical devices
ISO 13485 - Quality management systems
IEC 60601 - Medical electrical equipment
IEC 62366 - Application of usability engineering to medical devices
