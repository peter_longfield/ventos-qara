# HE-xxx VentOS
## Customer Statement Of Need

### 1. Use
The approval and release of this document shall follow HE-QS-SOP004.

### 2. Purpose and Scope
For further information refer to:
https://www.nngroup.com/articles/user-need-statements/

### 3. Customer Statement of Need
Developers and manufacturers of ventilators need high quality software to
control their products to meet the needs of the ventilator operators and 
assure regulators that the software complies with applicable standards.