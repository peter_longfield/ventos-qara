User requirements reviews.

Teams Interviewed
- PolyVent
- gVent 
- RespiraWorks
- COVS

Team Requirements (this should go in a separate design file)
- Various air drive mechanisms (bellows, blower, pneumatic, ambi-bag, gravity)
and sensor configurations (pressure, flow, oxygen).
- Teams have primarily focused on control systems
- Alarms are universal and well defined by IEC 60601-1-8
- A level of abstraction is required to interface with existing code bases
- System startup checks

