# HE-xxx VentOS
## Design and Development Review

### 1. Use
The approval and release of this document shall follow HE-QS-SOP004.

### 2. Purpose and Scope
This Design and Development Review aims to meet the requirements of the
HE-QS-QualityManual section:

7.3.5 Design and development review

Throughout the design and development process, reviews are conducted at
stages in accordance with the Quality Plan.

Design reviews evaluate the ability of the design to meet input requirements,
identify problems and propose solutions.

Design review participants include the designer(s), other members of the
cross-functional team, and independent reviewers and other specialists as
required.

All design reviews are documented, and form part of the Design History File (DHF).

HE-QS-SOP003-DesignControls

### 3. Formatting

All reviews must follow this format:

#### Review x
**Date:**  Date Month Year Time TimeZone

**Attendees:**
- x
- y
- z

**Notes:**  
1. note1
1. note2


### 4. Design and Development Reviews

#### Review 1
**Date:**  TBC

**Attendees:**
- x
- y
- z

**Notes:**  
1. note1
1. note2
